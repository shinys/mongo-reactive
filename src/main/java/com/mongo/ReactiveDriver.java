package com.mongo;

import com.mongodb.CursorType;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.async.client.MongoClientSettings;
import com.mongodb.connection.ClusterConnectionMode;
import com.mongodb.reactivestreams.client.MongoDatabase;
import com.mongodb.connection.ClusterSettings;
import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoClients;
import static java.util.Arrays.asList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicLong;
import org.bson.Document;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

/**
 *
 * @author younsshin
 */
public class ReactiveDriver {
    
    private static MongoDatabase database;
    
    final static CountDownLatch latch = new CountDownLatch(1);
    /**
     * https://mongodb.github.io/mongo-java-driver-reactivestreams/1.12/getting-started/quick-tour/
     * @param args 
     */
    public static void main(String[] args) throws InterruptedException {
        MongoCredential credential = MongoCredential.createScramSha1Credential("webuser", "entries", "password1234".toCharArray());
        ClusterSettings clusterSettings = ClusterSettings.builder()
                .hosts(asList(new ServerAddress("ds233258.mlab.com",33258)))
                .mode(ClusterConnectionMode.SINGLE)
                .build();
        MongoClientSettings settings = MongoClientSettings.builder()
                .clusterSettings(clusterSettings)
                .credential(credential)
                //.credentialList(Arrays.asList(credential))
                .build();
        
        //MongoClient가 AutoCloseable을 구현하고 있어 try-with-reaource 한거니 이렇게 쓰지않으면 종료 전 mongoClient.close() 할 것.
        try( MongoClient mongoClient = MongoClients.create(settings) ){
            database = mongoClient.getDatabase("entries");

            reactiveFindTest();

            // non-blocking 완료 대기
            latch.await();
        }

    }
    
    
    public static void reactiveFindTest(){
        //단순 갯수 확인용
        final AtomicLong cnt = new AtomicLong(0L);
        
        database.getCollection("testData").find() //.skip(0).limit(200)
                .cursorType(CursorType.NonTailable)
                .subscribe( new Subscriber<Document>(){
                    //https://www.reactive-streams.org/reactive-streams-1.0.0-javadoc/org/reactivestreams/Subscriber.html
                    @Override                    
                    public void onSubscribe(Subscription s) {
                        System.out.println("onSubscribe: "+ s.toString());
                        s.request(Long.MAX_VALUE);
                    }

                    @Override
                    public void onNext(Document t) {
                        cnt.incrementAndGet();
                        System.out.println("onNext: "+t.toJson());
                    }

                    @Override
                    public void onError(Throwable thrwbl) {
                        thrwbl.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                        System.out.println("onComplete!! "+ cnt.get()+"건");
                        latch.countDown();
                    }
                } );
        
    }
    
}
