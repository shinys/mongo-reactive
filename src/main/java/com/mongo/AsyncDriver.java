package com.mongo;

import com.mongodb.Block;
import com.mongodb.CursorType;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.async.SingleResultCallback;
import com.mongodb.async.client.MongoClient;
import com.mongodb.async.client.MongoClientSettings;
import com.mongodb.async.client.MongoClients;
import com.mongodb.async.client.MongoDatabase;
import com.mongodb.connection.ClusterConnectionMode;
import com.mongodb.connection.ClusterSettings;
import java.time.Duration;
import java.time.Instant;
import static java.util.Arrays.asList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicLong;
import org.bson.Document;

/**
 *
 * @author younsshin
 */
public class AsyncDriver {
    private static MongoDatabase database;
    final static CountDownLatch latch = new CountDownLatch(1);
    
    /**
     * https://mongodb.github.io/mongo-java-driver/3.0/driver-async/getting-started/quick-tour/
     * @param args 
     */
    public static void main(String[] args) throws InterruptedException {
        
        MongoCredential credential = MongoCredential.createScramSha1Credential("webuser", "entries", "password1234".toCharArray());
        ClusterSettings clusterSettings = ClusterSettings.builder()
                .hosts(asList(new ServerAddress("ds233258.mlab.com",33258)))
                .mode(ClusterConnectionMode.SINGLE)
                .build();
        MongoClientSettings settings = MongoClientSettings.builder()
                .clusterSettings(clusterSettings)
                .credential(credential)
                //.credentialList(Arrays.asList(credential))
                .build();
        
        //MongoClient가 AutoCloseable을 구현하고 있어 try-with-reaource 한거니 이렇게 쓰지않으면 종료 전 mongoClient.close() 할 것.
        try( MongoClient mongoClient = MongoClients.create(settings) ){
            database = mongoClient.getDatabase("entries");
            
            Instant start = Instant.now();
            
            asyncFindTest();
            // async 처리완료 대기
            latch.await();
            
            Instant end = Instant.now();
            System.out.println(Duration.between(start, end));
        }
    }
    
    public static void asyncFindTest(){
        //단순 갯수 확인용
        final AtomicLong cnt = new AtomicLong(0L);
        
        database.getCollection("testData").find() //.limit(200)
                .cursorType(CursorType.NonTailable) //default cursor type
                .batchSize(100)
                .forEach(
                        new Block<Document>(){
                            @Override
                            public void apply(Document t) {
                                cnt.incrementAndGet();
                                System.out.println("apply: "+t.toJson());
                            }
                        }
                        , new SingleResultCallback<Void>(){
                            @Override
                            public void onResult(Void t, Throwable thrwbl) {
                                System.out.println("Operation Finished! "+ cnt.get()+"건");
                                latch.countDown();
                            }
                        }
                );
    }
}
