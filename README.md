#### async, reactivestream 방식으로 컬렉션 조회하는 방법.  

#### CountDownLatch 을 이용한 어플리케이션 종료 지연. 


#### 몽고 driver-async, driver-reactivestreams 라이브러리 의존관계.     
![Alt 의존 관계](./document/dependency.png)  


#### Reactivestreams Memory Consume Test  

테스트 조건  
> JVM: Java HotSpot(TM) 64-Bit Server VM (25.162-b12, mixed mode)  
  Java: version 1.8.0_162, vendor Oracle Corporation  
  단일 collection 내 2,261,406 문서를 전체 순환  
  어플리케이션 실행 jvm 옵션과 reactive-streams backpressure를 조절하면서 메모리 consume 확인.    


back pressure
~~~java
.subscribe( new Subscriber<Document>(){
    private Subscription _s;
    final long subscriptionCount = 1000L;  //backpressure 
    @Override                    
    public void onSubscribe(Subscription s) {
        System.out.println("onSubscribe: "+ s.toString());
        _s = s;
        _s.request(subscriptionCount);
    }

    @Override
    public void onNext(Document t) {
        cnt.incrementAndGet();
        
        if(cnt.get()%subscriptionCount==0L ){
            System.out.println(cnt.get());
            _s.request(subscriptionCount);
        }
    }

    @Override
    public void onError(Throwable thrwbl) {
        thrwbl.printStackTrace();
    }

    @Override
    public void onComplete() {
        System.out.println("onComplete!! "+ cnt.get()+"건");
        latch.countDown();
    }
} );
~~~


##### test 1

jvm heap 메모리 설정 없이 backpressure 1000 으로 실행.



![Alt heap 메모리 설정 없이 back-pressure 1000](./document/no-heap-options-and-backpresure-1000.png)  


Full GC가 발생하기 직전에 처리가완료되어 reactive-streams 결과 consume이 메모리에 전체 load 되는건지 아닌건지 확인 하지 못해 다음 설정으로 추가 테스트



##### test 2

jvm -Xms150m -Xmx150m, backpressure 2000 으로 실행.  
 
![Alt -Xms150m -Xmx150m, back-pressure 2000](./document/heap-150-and-backpressure-2000.png)

메모리 제약하에 GC를 발생하며 잘 처리하는것 확인 함.  
그럼.. backpressure를 Long.MAX_VALUE 줘도 잘 작동 하겠네?  


##### test 3

jvm -Xms150m -Xmx150m, backpressure Long.MAX_VALUE 으로 실행.    

실행과 동시에 GC overhead limit exceeded 예외발생.    
비동기,non-blocking 처리이므로 어플리케이션이 죽지는 않음.     

~~~java
10월 15, 2019 11:21:33 오전 com.mongodb.diagnostics.logging.JULLogger log
정보: Cluster created with settings {hosts=[10.250.67.209:27017], mode=SINGLE, requiredClusterType=UNKNOWN, serverSelectionTimeout='30000 ms', maxWaitQueueSize=500}
onSubscribe: com.mongodb.reactivestreams.client.ObservableToPublisher$1$1@3a03464
10월 15, 2019 11:21:33 오전 com.mongodb.diagnostics.logging.JULLogger log
정보: No server chosen by com.mongodb.async.client.ClientSessionHelper$1@5cc7c2a6 from cluster description ClusterDescription{type=UNKNOWN, connectionMode=SINGLE, serverDescriptions=[ServerDescription{address=10.250.67.209:27017, type=UNKNOWN, state=CONNECTING}]}. Waiting for 30000 ms before timing out
10월 15, 2019 11:21:34 오전 com.mongodb.diagnostics.logging.JULLogger log
정보: Opened connection [connectionId{localValue:1}] to 10.250.67.209:27017
10월 15, 2019 11:21:34 오전 com.mongodb.diagnostics.logging.JULLogger log
정보: Monitor thread successfully connected to server with description ServerDescription{address=10.250.67.209:27017, type=SHARD_ROUTER, state=CONNECTED, ok=true, version=ServerVersion{versionList=[3, 6, 3]}, minWireVersion=0, maxWireVersion=6, maxDocumentSize=16777216, logicalSessionTimeoutMinutes=30, roundTripTimeNanos=38188919}
10월 15, 2019 11:21:34 오전 com.mongodb.diagnostics.logging.JULLogger log
정보: Opened connection [connectionId{localValue:2}] to 10.250.67.209:27017
java.lang.OutOfMemoryError: GC overhead limit exceeded
	at org.bson.io.ByteBufferBsonInput.readString(ByteBufferBsonInput.java:160)
	at org.bson.io.ByteBufferBsonInput.readCString(ByteBufferBsonInput.java:139)
	at org.bson.BsonBinaryReader.readBsonType(BsonBinaryReader.java:122)
	at org.bson.codecs.DocumentCodec.decode(DocumentCodec.java:149)
	at org.bson.codecs.DocumentCodec.decode(DocumentCodec.java:45)
	at com.mongodb.operation.CommandResultArrayCodec.decode(CommandResultArrayCodec.java:52)
	at com.mongodb.operation.CommandResultDocumentCodec.readValue(CommandResultDocumentCodec.java:60)
	at org.bson.codecs.BsonDocumentCodec.decode(BsonDocumentCodec.java:84)
	at org.bson.codecs.BsonDocumentCodec.decode(BsonDocumentCodec.java:41)
	at org.bson.codecs.configuration.LazyCodec.decode(LazyCodec.java:47)
	at org.bson.codecs.BsonDocumentCodec.readValue(BsonDocumentCodec.java:101)
	at com.mongodb.operation.CommandResultDocumentCodec.readValue(CommandResultDocumentCodec.java:63)
	at org.bson.codecs.BsonDocumentCodec.decode(BsonDocumentCodec.java:84)
	at org.bson.codecs.BsonDocumentCodec.decode(BsonDocumentCodec.java:41)
	at com.mongodb.connection.ReplyMessage.<init>(ReplyMessage.java:51)
	at com.mongodb.connection.InternalStreamConnection$2$1.onResult(InternalStreamConnection.java:379)
	at com.mongodb.connection.InternalStreamConnection$2$1.onResult(InternalStreamConnection.java:359)
	at com.mongodb.connection.InternalStreamConnection$MessageHeaderCallback$MessageCallback.onResult(InternalStreamConnection.java:651)
	at com.mongodb.connection.InternalStreamConnection$MessageHeaderCallback$MessageCallback.onResult(InternalStreamConnection.java:618)
	at com.mongodb.connection.InternalStreamConnection$5.completed(InternalStreamConnection.java:487)
	at com.mongodb.connection.InternalStreamConnection$5.completed(InternalStreamConnection.java:484)
	at com.mongodb.connection.AsynchronousSocketChannelStream$BasicCompletionHandler.completed(AsynchronousSocketChannelStream.java:233)
	at com.mongodb.connection.AsynchronousSocketChannelStream$BasicCompletionHandler.completed(AsynchronousSocketChannelStream.java:216)
	at sun.nio.ch.Invoker.invokeUnchecked(Invoker.java:126)
	at sun.nio.ch.Invoker.invokeUnchecked(Invoker.java:281)
	at sun.nio.ch.WindowsAsynchronousSocketChannelImpl$ReadTask.completed(WindowsAsynchronousSocketChannelImpl.java:579)
	at sun.nio.ch.Iocp$EventHandlerTask.run(Iocp.java:397)
	at sun.nio.ch.AsynchronousChannelGroupImpl$1.run(AsynchronousChannelGroupImpl.java:112)
	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1149)
	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
	at java.lang.Thread.run(Thread.java:748)

~~~

초기 소진 메모리 부족으로 발생한것이므로 heap 약간 더 할당하여 test4 수행  


##### test 4

jvm -Xms300m -Xmx300m, backpressure Long.MAX_VALUE 으로 실행.  
 
![Alt -Xms300m -Xmx300m, back-pressure Long.MAX_VALUE](./document/heap-300-and-backpressure-longmax.png)

![Alt -Xms300m -Xmx300m, back-pressure Long.MAX_VALUE](./document/cpu-heap-300-and-backpressure-longmax.png)

예상한대로 제한된 메모리내에서 잘 처리하는것 확인.  

그래프를 통해 초기 소진 메모리는 200Mb를 약간 상회하므로 이보다 여유를 두고 heap 설정 필요.  


  
    

##### 사족 
원래 이 테스트는 다음과 같은 프로파일링에서 시작했음.  
heap 설정없이 backpressure  Long.MAX_VALUE으로 돌렸더니 다음처럼 메모리를 소진하는게 아닌가?   
결과 stream을 모두 메모리에 적재하나? 뭔가 X되는 느낌? 그래서 위와 같은 테스트를 수행함.  
<details>
  <summary>수행하곤 깜놀해 바로 중지 함</summary>
![Alt 메모리 설정없이 backpressure Long.MAX_VALUE](./document/no-heap-options-and-backpresure-longmax.png)
</details>

추가로, java 11 빌드 후 jvm -Xms300m -Xmx300m, backpressure Long.MAX_VALUE 실행.    
실행 시간은 약간 주는 수준이나 CPU useage, GC activity 향상.    

![Alt -Xms300m -Xmx300m, back-pressure Long.MAX_VALUE](./document/java11-heap-300-and-backpressure-longmax.png)

![Alt -Xms300m -Xmx300m, back-pressure Long.MAX_VALUE](./document/java11-cpu-heap-300-and-backpressure-longmax.png)



